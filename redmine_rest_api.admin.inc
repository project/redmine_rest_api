<?php

/**
 * @file
 * Admin page callbacks for the redmine_rest_api module.
 */

/**
 * Form constructor for the redmine_rest_api configuration form.
 *
 * @see redmine_rest_api_admin_configure_submit
 * @ingroup forms
 */
function redmine_rest_api_admin_configure() {
  $form = array();
  $form['redmine_rest_api_redmine_version'] = array(
    '#title' => t('Redmine major version'),
    '#description' => t('For example, if you use 2.3.0 then enter 2.3. This must be correct, as the API changes slightly from version to version.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('redmine_rest_api_redmine_version', ''),
    '#required' => TRUE,
  );
  $form['redmine_rest_api_api_key'] = array(
    '#title' => t('Default Redmine API key'),
    '#description' => t('You can find your Redmine API key under My Account in Redmine.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('redmine_rest_api_api_key', ''),
    '#required' => TRUE,
  );
  $form['redmine_rest_api_redmine_base_url'] = array(
    '#title' => t('Redmine base URL'),
    '#description' => t('The URL of your Redmine instance, without trailing slash (note, configuring Redmine to accept https is advised).'),
    '#type' => 'textfield',
    '#default_value' => variable_get('redmine_rest_api_redmine_base_url', ''),
    '#required' => TRUE,
  );
  $form['redmine_rest_api_api_warnings'] = array(
    '#title' => t('Redmine API warnings'),
    '#description' => t('For debugging purposes the module can warn you if it receives an empty response from a Redmine API call.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('redmine_rest_api_api_warnings', FALSE),
  );

  return system_settings_form($form);
}

/**
 * Form for posting any RESTful API call to Redmine with the
 * default API key to view the data that comes back.
 *
 * @see redmine_rest_api_api_call_submit
 * @ingroup forms
 */
function redmine_rest_api_api_call() {
  $form = array();
  $form['redmine_rest_api_api_call_url'] = array(
    '#title' => t('Try URL'),
    '#description' => t('The RESTful API URL you would like to test, e.g. issues/123 for individual issue data. DO NOT add the .json or any URL parameters, this will not work.'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  $form['actions'] = array(
    '#tree' => FALSE,
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Try'),
  );
  return $form;
}

/**
 * Form submit handler for posting RESTful call to Redmine.
 *
 * @see redmine_rest_api_api_call
 */
function redmine_rest_api_api_call_submit($form, &$form_state) {
  // Try to get JSON data.
  $result = redmine_rest_api_call($form_state['values']['redmine_rest_api_api_call_url']);

  // $result will be NULL if there was no JSON data in returned HTTP result.
  if ($result) {
    if (module_exists('devel')) {
      dpm($result);
    }
    else {
      drupal_set_message(t('Enable the devel module to use this feature.'), 'error');
    }
  }
  else {
    drupal_set_message(t('No JSON received. See the Drupal log for more information.'), 'error');
  }
}

/**
 * Test page for trying out any Redmine API call to the REST API
 * with JSON. Also drives the Redmine API test page that loads
 * up project info to confirm details are OK.
 *
 * @see redmine_rest_api_fetch
 */
function redmine_rest_api_admin_test($fetch) {
  // For reference only.
  $api_key = variable_get('redmine_rest_api_api_key', '');
  $base_redmine = variable_get('redmine_rest_api_redmine_base_url', '');

  // Build URL part to fetch.
  $fetch = 'projects';
  // Try to get JSON data.
  $result = redmine_rest_api_call($fetch);

  // $result will be NULL if there was no JSON data in returned HTTP result.
  if ($result->decoded_data) {
    // Only bother showing developer information to user roles who have devel access.
    if (user_access('access devel information')) {
      drupal_set_message(t('Success fetching %url', array('%url' => $base_redmine . '/' . $fetch . '.json?key=' . $api_key)));
    }
    return '<pre>' . print_r($result->decoded_data, TRUE) . '</pre>';
  }
  else {
    return t('No JSON data received. Please check the data in the messages above carefully.');
  }
}
